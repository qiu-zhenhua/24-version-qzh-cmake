find_package(OpenCV)

aux_source_directory(src ASSEMBLY2_DIR)
add_library(assembly2 ${ASSEMBLY2_DIR})

target_include_directories(assembly2 
PUBLIC
    include
    ${OpenCV_INCLUDE_DIRS}
)
target_link_libraries(assembly2
    ${OpenCV_LIB}
)