/**
 * @file ort.cpp
 * @author SCUT RobotLab Vision Group
 * @brief OrtParam module source file (Generated by CMake automatically)
 * @version 1.0
 * @date 2023-08-18
 * 
 * @copyright Copyright 2023 (c), SCUT RobotLab Vision Group
 * 
 */

#include <opencv2/core/persistence.hpp>

#include "srvlpara/ort.hpp"

para::OrtParam::OrtParam(const std::string &path)
{
    cv::FileStorage fs(path, cv::FileStorage::READ);
    SRVL_Assert(fs.isOpened());

    para::readExcludeNone(fs["RGB_MEANS"], RGB_MEANS);
    para::readExcludeNone(fs["RGB_STDS"], RGB_STDS);
    para::readExcludeNone(fs["MONO_MEANS"], MONO_MEANS);
    para::readExcludeNone(fs["MONO_STDS"], MONO_STDS);

}
